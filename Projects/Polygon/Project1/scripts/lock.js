const hre = require("hardhat");
require("dotenv").config();
const { ALCHEMY_API_KEY, PRIVATE_KEY, CONTRACT_ADDRESS } = process.env;

const ContractJson = require("../artifacts/contracts/Lock.sol/Lock.json");
const abi = ContractJson.abi;

const main = async() => {
    const alchemy = new hre.ethers.providers.AlchemyProvider(
        'maticmum',
        ALCHEMY_API_KEY
    );

    const userWallet = new hre.ethers.Wallet(PRIVATE_KEY, alchemy);

    const Lock = new hre.ethers.Contract(
        CONTRACT_ADDRESS,
        abi,
        userWallet
    );

    await Lock.withdraw();
}


main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    })