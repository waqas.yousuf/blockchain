// Conctract deployed at: 0x9F486B54e7ecD6497eF5e56DeCB2C38a2c6Db0a9

const { ethers } = require("hardhat");

let contractAbi;

const main = async () => {
    const MyContract = await ethers.getContractFactory("EthereumWallet");
    contractAbi = await MyContract.deploy();
    await contractAbi.deployed();
}

const myMain = async() => {
    try
    {
        await main();
        console.log(`Conctract deployed at: ${contractAbi.address}`);
        process.exit(0);
    }
    catch(error)
    {   
        console.error(error);
        process.exit(1);
    }
}

myMain();