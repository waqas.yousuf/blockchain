const { ethers }  = require("hardhat");
const { abi } = require("../artifacts/contracts/EthereumWallet.sol/EthereumWallet.json");

require("dotenv").config();

const main  = async() => {

    const { CONTRACT_ADDRESS, GOERLI_API_KEY, ACCOUNT } = process.env;
    
    const contractAddress = CONTRACT_ADDRESS;

    // Provider
    const provider = new ethers.providers.AlchemyProvider(
        "goerli",
        GOERLI_API_KEY
    );

    // Signer
    const signer = new ethers.Wallet(
        ACCOUNT,
        provider
    );

    const myContract = new ethers.Contract(
        contractAddress,
        abi,
        signer
    );

    const value = ethers.utils.parseEther("0.2");
    const options = {value};
    // await myContract.depositEthers(options);
    await myContract.withdraw();
    // console.log(await provider.getBalance(CONTRACT_ADDRESS));

}

main()
    .catch((error) => {
        console.error(error);
    });