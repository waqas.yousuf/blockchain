const { ethers } = require("hardhat");
const { expect } = require("chai");
const { parseEther } = require('@ethersproject/units');

describe("Main contract logic testing", () => {

    let contractAbi;

    before(async() => {
        const MyContract = await ethers.getContractFactory("EthereumWallet");
        contractAbi = await MyContract.deploy();
        await contractAbi.deployed();
        console.log(`Conctract deployed at: ${contractAbi.address}`);
    });

    it("Deposit ethers in contract", async() => {

        const value = parseEther("1");
        options = { value: value};
        
        const accounts = await ethers.getSigners();
        const msgSender = accounts[0];
        const wallet = await contractAbi.connect(msgSender);

        await wallet.depositEthers(options);

        console.log(await contractAbi.getBalance());

    })



});