// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "hardhat/console.sol";

contract EthereumWallet
{

    address payable public owner;

    modifier onlyOwner()
    {
        require(msg.sender == owner, "Only owner can execute the function");
        _;
    }

    constructor()
    {
        owner = payable(msg.sender);
    }

    receive() external payable {}

    function depositEthers() payable external
    {
        (bool success,) = address(this).call{value: msg.value}("");
        require(success, "unable to process the deposit");
    }

    function withdraw() external payable onlyOwner
    {
        (bool success,) = owner.call{value: address(this).balance}("");
        require(success, "Unable to process the withdrawal");
    }

    function getBalance() external view returns (uint)
    {
        return address(this).balance;
    }


}