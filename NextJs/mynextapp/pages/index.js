import Link from 'next/link';
import styles from '../styles/Index.module.css'

export const getStaticProps = async () => {
  const res = await fetch("https://jsonplaceholder.typicode.com/posts");
  const data = await res.json();
  console.log(res);
  return {
    props: {
      data
    }
  }
}


const Index = ({ data }) => {
    console.log(data);

    return(
        <>
            <nav className={styles.khaternaak}>
                <Link href="/">Home</Link>
                <Link href="/contact">Contact Us</Link>
                <Link href="/about">About Us</Link>
            </nav>
            <h1>This is home page</h1>
        </>
    );

}

export default Index;