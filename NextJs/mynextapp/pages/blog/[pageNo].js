import { useRouter } from 'next/router';
import Link from 'next/link';

const pageNo = () => {
    const router = useRouter();
    const pageNumber = router.query.pageNo;


    return(
        <>
            <nav>
                <Link href="/">Home</Link>
                <Link href="/contact">Contact Us</Link>
                <Link href="/about">About Us</Link>
            </nav>
            <h1>This is the content of blog {pageNumber}</h1>
        </>
    );

}

export default pageNo;