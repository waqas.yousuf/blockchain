import React from 'react';
import './App.css';
import useCounter from './useCounter';

function App() {

  const [count, increment, decrement] = useCounter();


  return (
    <div className="App">
      <div>{count}</div>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
    </div>
  );
}

export default App;
