import React, { useContext } from 'react';
import { data } from './App';


export const ChildA = () => {
  const myName = useContext(data);
  return (
    <h2>Hello {myName}</h2>
  );
}
