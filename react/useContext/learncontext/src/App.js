import './App.css';
import { ChildA } from './ChildA'
import { createContext } from 'react';

const data = createContext();
const myName = "Muhammad";

function App() {
  return (
    <data.Provider value={myName}>
      <div className="App">
        <ChildA />
      </div>
    </data.Provider>
  );
}

export default App;
export { data };
