export const PersonList = [
    {
        id: 1,
        name: "Furqan",
        age: 49,
    },
    {
        id: 2,
        name: "Zahoor",
        age: 59,
    },
    {
        id: 3,
        name: "Majid",
        age: 39,
    },
];