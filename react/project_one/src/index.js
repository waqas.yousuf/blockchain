import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {PersonList} from './PersonList';

const myObj = {
  name: 'Waqas',
  age: 35,
  height: 5.7
}

const myArr = ["Baloons", "Bats", "Balls"];

const myButtonClicked = () => {
  alert("Hello Vakki");
};

function MyApp() {

  return (
    <>
      <Title name={myObj.name} age={myObj.age}>
        <p>This is first p tag in title</p>
        <p>This is second p tag in title</p>
      </Title>
      <div>{
        PersonList.map(
          (person) => {

            const {name, age, id} = person;

            return (
              <div key={id}>
                <h2 onClick={() => console.log(name) }>{name}</h2>
              </div>
            );
          }
        )
      }</div>
      <button onClick={myButtonClicked}>Click Me!</button>
    </>
  );

}

const Title = (props) => <h1>My name is {props.name} and I am {props.age} years old. The children in props: {props.children}</h1>;

ReactDOM.render(<MyApp />, document.getElementById('root'));