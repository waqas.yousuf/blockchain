import './App.css';
import { ShortCircuit } from './ShortCircuit';
import Setup from './PropDrilling';

function App() {
  return (
    <div className="App">
	    <Setup />
    </div>
  );
}

export default App;
