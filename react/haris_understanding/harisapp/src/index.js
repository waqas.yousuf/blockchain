import ReactDOM from 'react-dom';


const my_obj = {
  name: "Waqas",
  age: 34,
  location: "Karachi"
}

const App = () => {
  return (
    <>
    <Item title="My Title">
      <h4>I am a child prop</h4>
      <p>I am second child prop</p>
      </Item>
    <Item title={my_obj.name}>
      <h4>I am a child prop</h4>
      <p>I am second child prop</p>
    </Item>
    <Item title="My Title3">
      <h4>I am a child prop</h4>
      <p>I am second child prop</p>
    </Item>
    </>
  )
}


// const Item = (props) => <h1>Hello Haris</h1>

const Item = (props) => {
  return (
    <>
      <h1>{props.title}</h1>
      <p>{props.children[1]}</p>
    </>
  )
}


ReactDOM.render(<App/>, document.getElementById("root"))



