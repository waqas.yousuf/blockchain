// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract MyEvents{

    event myLog(address indexed sender, string  indexed message, uint id, uint seq);
    event event2();


    function testEvents() public {
        emit myLog(msg.sender, "Blockchain is the future", 47, 4);
        emit myLog(msg.sender, "User registered successfully", 48, 5);
        emit event2();
    }


}
