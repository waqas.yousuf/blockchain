// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract MyMappings
{

    struct Book{
        string author;
        string title;
    }

    mapping (string => string) public myMap;
    mapping (uint => Book) public books;
    mapping (address => mapping(uint => Book)) public myBooks;

    constructor()
    {
        myMap["youngest"] = "Waqas";
        myMap["eldest"] = "Faisal";
        myMap["elder"] = "Mona";
        myMap["oldest"] = "Yousuf";
    }

    function addBook(uint _id, string memory _author, string memory _title) public
    {
        books[_id] = Book(_author, _title);
    }

    function addMyBook(uint _id, string memory _author, string memory _title) public
    {
        myBooks[msg.sender][_id] = Book(_author, _title);
    }


}
