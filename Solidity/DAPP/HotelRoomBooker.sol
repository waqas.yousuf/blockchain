// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

// this smart contract will be responsible for booking a hotel room
contract HotelRoom
{
    enum Statuses { Vacant, Occupied } Statuses public currentStatus;

    address payable public owner;

    constructor()
    {
        owner = payable(msg.sender);
        currentStatus = Statuses.Vacant;
    }

    // defining event
    event Occupy(address _occupant, uint _val);

    modifier onlyWhileVacant
    {
        require(currentStatus == Statuses.Vacant, "Room currently occupied");
        _; // this will return the function code
    }

    modifier onlyIfEnoughAmount(uint _amount)
    {
        require(msg.value >= _amount, "Not enough ether provided");
        _;
    }

    function bookRoom() public payable onlyWhileVacant onlyIfEnoughAmount(2 ether)
    {   
        currentStatus = Statuses.Occupied;

        owner.transfer(msg.value);
        (bool sent, bytes memory data) = owner.call{value: msg.value}("");
        require(true);

        emit Occupy(msg.sender, msg.value);
    }
}
