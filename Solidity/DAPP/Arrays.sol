// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


contract Arrays
{
    string[] public myStrArr = ["Apple", "Banana", "Oranges"];

    uint[][] public arr2D = [[1,2,3], [4,5,6]];

    function getStrArr() public view returns (string[] memory)
    {
        return myStrArr;
    }

    function kuchKro(uint _item) public
    {
        arr2D[0].push(_item);
    }
}
