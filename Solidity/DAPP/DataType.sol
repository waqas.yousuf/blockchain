// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract DataType
{
    bytes32 myString = "Hello World!!";

    function getChar() public view returns (bytes1)
    {
        return myString[3];
    }
}

