// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract Ownable
{

    address owner;
    modifier onlyOwner() {
        require(msg.sender == owner, "Access denied. You must be owner of the contract");
        _;
    }

    constructor()
    {
        owner = msg.sender;
    }

}

contract SecretVault
{

    string secret;
    constructor(string memory _secret)
    {
        secret = _secret;
    }

     function getSecret() public view returns (string memory)
    {
        return secret;
    }

}

contract MyContract is Ownable
{
    // declaring a variable to assign the address of the deployed contract
    address secretVaultAddress;
    
    constructor(string memory _secret)
    {
        // deploying the contract
        SecretVault _secretVault = new SecretVault(_secret);
        // saving the address of the deployed contract into a state variable
        secretVaultAddress = address(_secretVault);
        // calling the constructor of parent contract
        super;
    }

    function getSecret() public view onlyOwner returns (string memory)
    {
        return SecretVault(secretVaultAddress).getSecret();
    }

}
