// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;

contract AbiFunctions
{
    function abiEncoding(string memory _str1, uint _int, string memory _str2) public pure returns (bytes memory)
    {
        return abi.encode(_str1, _int, _str2);
    }

    function abiDecoding(bytes memory _data) public pure returns (string memory _str1, uint _int, string memory _str2)
    {
        (_str1, _int, _str2) = abi.decode(_data, (string, uint, string));
    }
}


