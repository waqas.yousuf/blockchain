//SPDX-License-Identifier: MIT
pragma solidity 0.8.6;


contract MyMapping
{

     struct Person{
        uint id;
        string name;
        uint age;
    }

    Person public person;

    mapping (uint => Person) myMap;

    function createPerson(uint _id, string memory _name, uint _age) public
    {   
        person = Person(_id, _name, _age);
    }

    function setMyMap(uint id) public
    {
        myMap[id] = person;
    }

    function getMyMap(uint _id) public view returns (Person memory) {
        return myMap[_id];
    }


}

