const Web3 = require('web3');
const abi = require("./abi.json");

const ALCHEMY_URL = "https://eth-mainnet.g.alchemy.com/v2/vyG4j2PKhTFOTEu5MmL-V4HvwXualaia";
const web3 = new Web3(ALCHEMY_URL);
const contractAdress = "0x6B175474E89094C44Da98b954EedeAC495271d0F";

const main = async() => {
    const latest = await web3.eth.getBlockNumber();
    const contract = new web3.eth.Contract(abi, contractAdress);

    contract.events.Transfer(
        {
            filter: {
                src: "0xe1aB0442520D5b8bCf09317aD7E0C5bAD5824fcd"
            }
        },
        (error, log)=>{
            if(error){
                console.log("Error", error);
            }
            console.log("Log", log);
        }
    );

    // const logs = await contract.getPastEvents("Transfer", {
    //     formBlock: latest - 10000,
    //     toBlock: latest,
    //     filter: {
    //         dst: "0xA478c2975Ab1Ea89e8196811F51A7B7Ade33eB11"
    //     }
    // });
    // console.log(logs);
} 

main().catch((error) => {
    console.log(error);
})

// 0xd6bC5433a7a8CB6A735D63C890C863fc1DCb6870