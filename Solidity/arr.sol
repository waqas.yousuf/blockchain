//SPDX-License-Identifier: MIT
pragma solidity 0.8.6;




contract LearnArrays
{

    uint[] myArr = [23, 45, 67, 88, 98, 65];

    function updateArray(uint num) public
    {
        myArr.push(num);
    }
    
    function getArray() public view returns (uint[] memory)
    {
        return myArr;
    }

    function popArray() public
    {
        myArr.pop();
    }

    function getLength() public view returns (uint) 
    {
        return myArr.length;
    }

    function remIndEle(uint ind) public
    {
        delete myArr[ind];
    }

    function completeAssign() public returns (uint)
    {
        uint targetInd;
        for(uint i=0; i<myArr.length; i++)
        {
            uint myEle = myArr[i];
            if(myEle == 0)
            {
                targetInd = i;
                break;
            }
        }

        for(uint i=targetInd; i<myArr.length; i++)
        {
            if(i == (myArr.length-1))
            {
                myArr.pop();
            }
            else
            {
                myArr[i] = myArr[i+1];
            }
        }            
    }

}
