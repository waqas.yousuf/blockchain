//SPDX-License-Identifier: MIT
pragma solidity 0.8.6;

contract MyStruct
{

    // creating user defined data type
    struct myStruct{
        uint id;
        string name;
        uint age;
    }

    // declaring and defining variable <person> that is of myStruct data type
    myStruct public person;
    
    function setPerson(uint _id, string memory _name, uint _age) public
    {
        person = myStruct(_id, _name, _age);
    }

    function getPerson() public view returns (myStruct memory)
    {
        return person;
    }

}
