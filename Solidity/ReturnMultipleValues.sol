// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


contract MultipleValues{

    struct myStruct{
        uint id;
        string name;
    }

    mapping (uint => string) myMap;

    constructor() {
        myMap[47] = "Waqas";
    }

    function returnValues() public pure returns (uint, bool, uint8[3] memory, myStruct memory, string memory) {
        return (23, true, [1, 2, 3], myStruct(7, "Waqas"), "Blockchain is the future of tech");
    }

    
    function callingReturnNamedVariables() public pure returns (uint, string memory, bool) {
        (uint a, string memory b, bool c) = returnNamedVariables();
        return (a, b, c);
    }


    function returnNamedVariables() public pure returns (uint x, string memory y, bool z) {
        x = 30;
        y = "Waqas";
        z = true;
        return(47, "Waqas", true);
    }

    function defaultValuesAssigned() public pure returns (uint x, bool y, string memory z) {
        return (x, y, z);
    }

    function myDestructuring() external pure returns (uint, uint) {
        // (uint a, uint b, uint c) = (10, 20, 30);
        (uint a,, uint c) = (10, 20, 30);
        a = 100;
        return (a, c);
    }

    function returningDefaultValues() external pure returns (uint x, bool y, string memory z, address) {
        (x,y,z);
    }


}

contract CheckingExternalCalls{

    MultipleValues myContract;

    constructor() {
        myContract = new MultipleValues();
    }

    function callingReturnNamedVariables() public view returns (uint, string memory, bool) {
        (uint a, string memory b, bool c) = myContract.returnNamedVariables();
        return (a, b, c);
    }
}

contract JsObjectTypeDataType{
    function funcA(address c, bool d) public pure returns (address, bool) {
        return (c, d);
    }

    function callFunctionWithKeyValue() external pure returns (address, bool) {
        return (funcA({c: address(0), d:true})); // way of passing named arguments
    }


}
