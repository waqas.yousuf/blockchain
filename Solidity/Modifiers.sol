// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract LearningModifiers{
    uint public num;

    constructor(){
        num = 10;
    }

    modifier validateBeforeUpdate(uint _num){
        if(_num >= 5){
            _;
        }
    }

    function update(uint _num) validateBeforeUpdate(_num) public {
        num += 5;
    }

}
