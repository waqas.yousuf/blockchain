// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;



contract CallSendTransfer
{
    function sendEtherTransfer(address payable _to) public payable 
    {
        // this is not recommended
        _to.transfer(msg.value); // 2300 max gas limit
    }

    function sendEtherSend(address payable _to) public payable 
    {
        bool sent = _to.send(msg.value); // 2300 gas limit
        require(sent, "Ethers not sent");
    }

    function sendEtherCall(address payable _to) public payable 
    {
        (bool sent, ) = _to.call{gas: 10000, value: msg.value}("");
        require(sent, "failed to send ether");
    }

}
