// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;


interface CalcTemplate 
{
    function op() external view returns (uint);
}

abstract contract CalcTemplate2
{
    function op() virtual public view returns (uint);

    function getValue() public pure returns (uint)
    {
        return 1;
    }
}

contract Add is CalcTemplate
{
    uint public num1;
    uint public num2;

    function op() override public view returns (uint) 
    {
        return num1 + num2;
    }
}
