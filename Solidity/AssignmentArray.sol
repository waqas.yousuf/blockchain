//SPDX-License-Identifier: MIT
pragma solidity 0.8.6;

contract LearnArrays
{

    uint[] myArr = [23, 45, 67, 88, 98, 65];

    // append element to the array
    function updateArray(uint num) public
    {
        myArr.push(num);
    }
    
    // get the array
    function getArray() public view returns (uint[] memory)
    {
        return myArr;
    }

    // remove last element from the array
    function popArray() public
    {
        myArr.pop();
    }

    // get length of array
    function getLength() public view returns (uint) 
    {
        return myArr.length;
    }

    // remove element from specific index
    function remIndEle(uint ind) public
    {
        delete myArr[ind];
    }

    // after removing any element from middle of an array shift all the element to left
    function completeAssign() public returns (uint)
    {
        uint targetInd;
        for(uint i=0; i<myArr.length; i++)
        {
            uint myEle = myArr[i];
            if(myEle == 0)
            {
                targetInd = i;
                break;
            }
        }

        for(uint i=targetInd; i<myArr.length; i++)
        {
            if(i == (myArr.length-1))
            {
                myArr.pop();
            }
            else
            {
                myArr[i] = myArr[i+1];
            }
        }            
    }

}
