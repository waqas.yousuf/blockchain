//SPDX-License-Identifier: MIT

pragma solidity 0.8.6;

contract structure
{
    struct Book{
        string title;
        int price;
        string author;
    }

    Book my_book;

    function setBook() public
    {
        my_book = Book("React JS", 50, "Haris Sarani");
    }

    function getPrice() public view returns(int)
    {
        return my_book.price;
    }

}
