// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract TransferBalance{

    mapping(address => uint) public balances;
    address public owner;

    modifier validateBalance(uint _amount)
    {
        require(balances[msg.sender] >= _amount, "Insufficient Balance!");
        _;
    }

    modifier validateOwner() {
        require(msg.sender == owner, "Please stay in your limits");
        _;
    }

    modifier validateTransaction(address _to)
    {
        require(msg.sender != _to, "Receiver's address must be different");
        _;
    }

    constructor() 
    {
        owner = msg.sender;
    }

    function addBalance(uint _amount) validateOwner public 
    {
        balances[msg.sender] += _amount;
    }

    function getBalance() public view returns (uint) 
    {
        return balances[msg.sender];
    }

    function transferAmount(address _to, uint _amount) validateBalance(_amount) validateTransaction(_to) public returns (uint) 
    {
        balances[msg.sender] -= _amount;
        balances[_to] += _amount;

        uint a = 4;
        uint b = 5;
        uint c = a + b;

        require(false, "check this revert");
        assert(c == 9);


        return balances[_to];
    }


}
