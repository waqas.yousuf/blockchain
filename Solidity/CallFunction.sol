// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;


contract CallFunction
{
    string public str = "Blockchain";

    modifier funded () {
        require(msg.value == 1 ether, "Failed");
        _;
    }

    // below function will receive ethers then will update the string
    function updateString(string memory _newString) funded public payable returns (uint,uint)
    {
        uint startGas1 = gasleft();

        str = _newString;
        address payable owner = payable(address(this));// payable(msg.sender);

        (bool success,) = owner.call{value: msg.value}("");
        require(success, "failure");

        return (startGas1, startGas1-gasleft());

    }
}
