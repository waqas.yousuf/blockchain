//SPDX-License-Identifier: MIT

pragma solidity 0.8.6;

contract enumerate
{

    // int my_num = 34;
    enum mylist{banana, phone, spoon}
    mylist item;

    function setSpoon() public
    {   
        item = mylist.spoon;
    }

    function getItem() public view returns (mylist)
    {
        return item;
    }

}
