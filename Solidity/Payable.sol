// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


contract Payable
{
    address payable public owner;

    constructor() payable
    {
        owner = payable(msg.sender);
    }

    // this function can receive ether(s)
    function deposit() public payable {}

    // this function cannot accept ethers
    function nonPayable() public {}

    function withdraw() public
    {
        uint amount = address(this).balance;
        (bool success,) = owner.call{value: amount}("amount withdrawn from smart contract");
        require(success, "Transaction failed");
    }

    function transfer(address payable _to, uint _amount) public
    {
        (bool success,) = _to.call{value: _amount}("ether transferred");
        require(success, "Failed to send ethers to address");
    }

}


