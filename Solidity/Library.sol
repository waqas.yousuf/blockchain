// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;

library ArrayFunctionsExtension
{
    function removeElement(uint[] storage _arr, uint _index) public
    {
        require(_arr.length > 0, "Provided array is empty");
        _arr[_index] = _arr[_arr.length - 1];
        _arr.pop();
    }
}

contract MyContract
{
    uint[] public arr;
    using ArrayFunctionsExtension for uint[];

    constructor()
    {
        for(uint i=0;i<4;i++)
        {
            arr.push(i+1);
        }
    }

    function remEle(uint _index) public returns (uint[] memory)
    {
        arr.removeElement(_index);
        return arr;
    }

}
