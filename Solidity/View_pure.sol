// SPDX-License-Identifier: MIT

pragma solidity 0.8.3;

contract ViewPure
{
    uint public x = 8;

    function pure_addx(uint y) public pure returns(uint)
    {
        uint z = 3 + y;
        return z;
    }

    function view_addx(uint y) public view returns(uint)
    {
        uint z = x + y;
        return z;
    }
}
