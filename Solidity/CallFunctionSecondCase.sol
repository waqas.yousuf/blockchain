// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;


contract Called
{
    event callEvent(address _sender, address _origin, address _from);

    function callThis() public
    {
        emit callEvent(msg.sender, tx.origin, address(this));
    }
}

contract Caller
{
    function makeCalls(address _contractAddress) public 
    {
        (bool success, ) = address(_contractAddress).call(abi.encodeWithSignature("callThis()"));
        require(success, "failed");
    }
}

/*

Caller:
"_sender": "0xEf9f1ACE83dfbB8f559Da621f4aEA72C6EB10eBf",
"_origin": "0x5B38Da6a701c568545dCfcB03FcB875f56beddC4",
"_from": "0x5A86858aA3b595FD6663c2296741eF4cd8BC4d01"

Called:
"_sender": "0x5B38Da6a701c568545dCfcB03FcB875f56beddC4",
"_origin": "0x5B38Da6a701c568545dCfcB03FcB875f56beddC4",
"_from": "0x5A86858aA3b595FD6663c2296741eF4cd8BC4d01"

*/
