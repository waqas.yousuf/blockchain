// I am actually trying to dive deep in events
// I have tried to use <contract> API instead of <provider> API

const ethers = require("ethers");
const abi = require("./abi.json");
require("dotenv").config();

const main = async() => {
    const usdtAddress = "0xdAC17F958D2ee523a2206206994597C13D831ec7";
    const provider = new ethers.providers.WebSocketProvider(`wss://eth-mainnet.g.alchemy.com/v2/${process.env.ALCHEMY_API_KEY}`);

    // Creating contract instance
    const contract = new ethers.Contract(
        usdtAddress,
        abi,
        provider
    );
    
    // Getting the latest block number
    const latest = await provider.getBlockNumber("latest");

    // **- interacting with Past Events -**
    
    // Creating an event object
    const myEvent = (await contract.queryFilter("Transfer", latest))[0];

    /* // logging the values of initial events, just for testing purpose
    const myValues = myEvents.slice(1,3).map((event) => {
        const value = ethers.utils.formatUnits(event.args.value, 6);
        return value;
    });*/
    
    // Subscribing to event
    const myListener = contract.on(myEvent, () => {
        console.log("Hello this is first listener");
    });

    /* // Subscribing to event
    const myListener2 = contract.on(myEvent, () => {
        console.log("Hello this is second listener");
    });*/

    // Getting the count of listeners subscribed to particular event
    console.log(contract.listenerCount(myEvent));

    // Removing event listener
    // contract.off(myEvent, myListener); // Not working
    // contract.removeAllListeners([myEvent]); // Not working
    contract.removeListener([myEvent]); // Not working
    
    // Getting the count of listeners subscribed to particular event after removing event listener
    console.log(contract.listenerCount(myEvent));

    

    // **- Listening to live events when emitted  -**

    /* // Setting the event listener
    contract.on(
        "Transfer",
        (from, to, value, event) => {
            let info = {
                from,
                to,
                value: ethers.utils.formatUnits(value, 6),
                data: event
            };
            console.log(JSON.stringify(info, null, 4));
        }
    )*/
    
}

main().catch((error)=>console.log(error));