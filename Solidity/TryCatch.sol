// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract A
{
    function doSomething() external pure
    {
        revert();
    }

}

contract B
{
    A aContract = new A();
    string public status = "Not Available";

    event tryEvent(string myMsg);

    function go() public
    {

        try aContract.doSomething()
        {
            status = "Success";       
        }
        catch 
        {
            emit tryEvent("Failed");
            // status = "Failed";
        }
        
    }

}

