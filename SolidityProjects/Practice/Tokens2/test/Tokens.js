const { ethers } = require('hardhat');
const { expect } = require('chai');

const deployMyContract = async (name) => {    
    const [ owner ] = await ethers.getSigners();
    const Token = await ethers.getContractFactory(name);
    const hhtoken = await Token.deploy();
    return hhtoken;
}

describe("Tokens Contract", () => {

    it("Once deployed owner should have 1000 tokens", async ()=>{
        
        const [ owner ] = await ethers.getSigners();
        const hhtoken = await deployMyContract("Tokens");
        const ownerBalance = await hhtoken.getBalanceOf(owner.address);

        expect(await hhtoken.totalSupply()).to.equal(ownerBalance);

    });

    it("Should transfer token between accounts", async ()=>{
        
        const [ owner, addr1, addr2 ] = await ethers.getSigners();
        const hhtoken = await deployMyContract("Tokens");

        await hhtoken.transferBalance(addr1.address, 100);

        const senderBalance = await hhtoken.getBalanceOf(owner.address);
        const receiverBalance = await hhtoken.getBalanceOf(addr1.address);
        
        expect(senderBalance).to.equal(900);
        expect(receiverBalance).to.equal(100);

    });

});