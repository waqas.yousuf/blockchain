// SPDX-License-Identifier: MIT
pragma solidity >= 0.7.0 < 0.9.0;

contract Tokens
{

    address public owner;
    uint public totalSupply = 1000;
    mapping(address => uint) balances;


    constructor()
    {
        owner = msg.sender;
        balances[owner] = totalSupply;
    }

    function transferBalance(address _to, uint _amount) external
    {
        balances[owner] -= _amount;
        balances[_to] += _amount;
    }

    function getBalanceOf(address _account) external view returns (uint)
    {
        return balances[_account];
    }

}
