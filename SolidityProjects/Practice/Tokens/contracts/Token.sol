// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;


contract Token
{
  string public name = "Ineuron Token";
  string public symbol = "INT";
  
  mapping(address => uint) balances;

  uint public totalSupply = 1000;
  address public owner;

  constructor() 
  {
    owner = msg.sender;
    balances[owner] = totalSupply;
  }

  function transfer(address _to, uint _amount) external
  {
    balances[msg.sender] -= _amount;
    balances[_to] += _amount;
  }

  function balanceOf(address _account) external view returns (uint)
  {
    return balances[_account];
  }

}

