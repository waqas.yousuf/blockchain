const { ethers } = require("hardhat");
const { expect } = require("chai");
describe("Token Contract", () => {
  it("Once Deployed, owner should have 1000 tokens", async () => {
    const [owner] = await ethers.getSigners();
    const Token = await ethers.getContractFactory("Token");
    const hhtoken = await Token.deploy();
    // console.log(hhtoken);

    const ownerBalance = await hhtoken.balanceOf(owner.address);    
    expect(await hhtoken.totalSupply()).to.equal(ownerBalance);

  });

  it("Should transfer token between accounts", async () => {
    const [ owner, addr1, addr2 ] = await ethers.getSigners();
    const Token = await ethers.getContractFactory("Token");
    const hhtoken = await Token.deploy();

    await hhtoken.transfer(addr1.address, 100);

    const bal1 = await hhtoken.balanceOf(owner.address);
    const bal2 = await hhtoken.balanceOf(addr1.address);

    expect(bal1).to.equal(900);
    expect(bal2).to.equal(100);

  });
  
});

