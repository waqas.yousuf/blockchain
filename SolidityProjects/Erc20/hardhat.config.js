require("@nomicfoundation/hardhat-toolbox");
require("@nomiclabs/hardhat-ethers")
require("hardhat-gas-reporter");

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: "0.8.6",
  gasReporter: {
    enabled: true,
    currency: "USD",
    // noColors: true,
    // outputFile: "gasReport.txt",
    coinmarketcap: "4403d20c-9317-44bd-ae14-eba31d8a9afc",
    // token: "matic"
  }
};
