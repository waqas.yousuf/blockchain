// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";


contract ICO is ERC20, Ownable {

    constructor() ERC20("Vicoin", "VIC") { // initializing a child constructor by using the properties of the parent contract
        
        _mint(msg.sender, 3000000000000000000000000000000000000000000000000); // this method is used to create new tokens
    
    }

    function myMint(address _account, uint _amount) external onlyOwner
    {
        require(_account != address(this) && _amount != uint256(0), "ERC20: function mint invalid input");
        _mint(_account, _amount);
    }

    function burnMyTokens(address _account, uint _amount) external onlyOwner
    {
        require(_account != address(this) && _amount != uint256(0), "ERC20: function mint invalid input");
        _burn(_account, _amount);
    }

    // function to buy crypto currency with ethers
    function buyCrypto() external payable
    {
        require(msg.sender.balance >= msg.value && msg.value != 0 ether, "ICO: function buy invalid input");
        uint amount = msg.value;
        _transfer(owner(), _msgSender(), amount);
    }

    function myWithdrawal () external onlyOwner payable
    {
        require(msg.value <= address(this).balance, "ICO: function withdraw has invalid input");
        payable(owner()).transfer(msg.value);
        // _transfer(address(this), _msgSender(), _amount);
    }

}

// first: 0x5B38Da6a701c568545dCfcB03FcB875f56beddC4
// owner address: 0xAb8483F64d9C6d1EcF9b849Ae677dD3315835cb2
// third address: 0x4B20993Bc481177ec7E8f571ceCaE8A9e22C02db