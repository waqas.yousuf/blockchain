const { ethers } = require("hardhat");
const { expect } = require("chai");

describe("Testing Erc20 tokens contract", () => {

    describe("Testing core operations", () => {
      
        let contractABI;
        let accounts;
        const amount = 3;

        before(async () => {
            const myContract = await ethers.getContractFactory("ICO");
            contractABI = await myContract.deploy();
            await contractABI.deployed();
            accounts = await ethers.getSigners();
        });

        it("Initial balance should be equal to total supply", async () => {
            const initialSupply = await contractABI.balanceOf(accounts[0].address);
            const totalSupply = await contractABI.totalSupply();
            expect(initialSupply).to.equal(totalSupply);
        });

        it("Do not have permission to mint tokens", async () => {
            const minter = accounts[0];
            const wallet = await contractABI.connect(minter);
            expect(await wallet.myMint(accounts[1].address, amount)).to.be.reverted;
        });

        it("Do not have permissions to burn tokens", async () => {
            const burner = accounts[0];
            const wallet = await contractABI.connect(burner);
            expect(await wallet.burnMyTokens(accounts[1].address, amount)).to.be.reverted;
        });

        it("Buy tokens with ethers", async () => {
            const buyer = accounts[2];
            const bigAmount = ethers.utils.parseEther("3");
            const options = {value: bigAmount};
            const wallet = contractABI.connect(buyer);
            await wallet.buyCrypto(options);
            const newBalanceOfBuyer = await wallet.balanceOf(buyer.address);
            const transferredTokens = options.value;
            expect(newBalanceOfBuyer).to.equal(transferredTokens);
        });

        it("you do not have persmision to withdraw ethers", async () => {
            const wallet = contractABI.connect(accounts[0]);
            options = {value: amount};
            expect(await wallet.myWithdrawal(options)).to.be.reverted; 
        });

        it("transfer amount to destination account", async () => {
            await contractABI.transfer(accounts[1].address, amount);
            expect(await contractABI.balanceOf(accounts[1].address)).to.equal(amount);
        });

        it("cannot transfer above the amount", async () => {
            const wallet = contractABI.connect(accounts[0]);
            expect(await wallet.transfer(accounts[1].address,1)).to.be.reverted;
        });

        it("test minting token", async () => {
            const tokensBeforeMint = await contractABI.balanceOf(accounts[0].address);
            await contractABI.myMint(accounts[0].address, amount);
            const tokensAfterMint = await contractABI.balanceOf(accounts[0].address);
            expect(tokensAfterMint).is.equal(tokensBeforeMint.add(amount));
        });

        it("test to burn tokens", async () => {
            const tokensBeforeBurn = await contractABI.balanceOf(accounts[0].address);
            await contractABI.burnMyTokens(accounts[0].address, amount);
            const tokensAfterBurn = await contractABI.balanceOf(accounts[0].address);
            expect(tokensAfterBurn).is.equal(tokensBeforeBurn.sub(amount));
        });
        
        it("withdraw ether from smart contract", async () => {
            const ownerBalanceBeforeWithdraw = await accounts[0].getBalance();
            console.log("Before withdraw", ownerBalanceBeforeWithdraw);
            const bigAmount = ethers.utils.parseEther("3");
            const options = {value: bigAmount};
            const wallet = contractABI.connect(accounts[0]);
            await wallet.myWithdrawal(options);
            const ownerBalanceAfterWithdraw = await accounts[0].getBalance();
            console.log("After withdraw", ownerBalanceAfterWithdraw);
            expect(ownerBalanceBeforeWithdraw.lt(ownerBalanceAfterWithdraw)).to.equal(true);
        });

        it("do not have enough ether to buy token", async () => {
            const wallet = contractABI.connect(accounts[3])
            const bigAmount = ethers.utils.parseEther("9999");
            const option = {value: bigAmount};
            let error;
            try
            {
                await wallet.buyCrypto(option);
            }catch(err)
            {
                error = "Sender does not have enough funds";
            }
            expect(error).to.equal("Sender does not have enough funds");
        });

        // it("Only owner can withdraw", async () => {
        //     const owner = accounts[0];
        //     const wallet = await contractABI.connect(owner);
        //     const contractBalance = await wallet.myWithdrawal();
        //     expect(contractBalance).to.equal("0.0");
        // });


    })

});
