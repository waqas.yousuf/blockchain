// SPDX-License-Identifier: MIT
pragma solidity ^0.8.6;

contract Fruits {
  string[] myFruits;

  function addFruit(string calldata _fruitName) external {
    myFruits.push(_fruitName);
  }

  function updateFruit(uint _index, string calldata _newname) external {
    if(myFruits.length > _index) {
      myFruits[_index] = _newname;
    }
  }

  function deleteFruit(uint _index) external {
    if(myFruits.length > _index) {
      delete myFruits[_index];
    }
  }

  function getFruits() external view returns (string[] memory) {
    return myFruits;
  }



}
