pragma solidity ^0.8.6;


import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Adoption.sol";

contract TestAdoption {
  // the address of the adoption contract to be tested
  Adoption adoption = Adoption(DeployedAddresses.Adoption());

  // id of the pet that we will use for testing
  uint expectedId = 5;

  // expected owner of the adopted pet is this contract
  address expectedAdopter = address(this);

  // testing the adopt function
  function testUserCanAdoptPet() external {
    uint returnedId = adoption.adopt(expectedId);
    Assert.equal(returnedId, expectedId, "Adoption of expected pet should match what is returned");
  }

  // testing retrieval of single pet owner
  function testGetAdoptedAddressByPetId() external {
    address adopter = adoption.adopters(expectedPetId);

    Assert.equal(adopter, expectedAdopter, "owner of the expected pet should match this contract");
  }

  // testing retrieval of all the pet owners
  function testGetAdopterAddressByPetIdInArray() external {
    address[10] memory adopters = adoption.getAdopters();
    Assert.equal(adopters[expectedPetId], expctedAdopter, "owner of the expected pet should be this contract");
  }

}

