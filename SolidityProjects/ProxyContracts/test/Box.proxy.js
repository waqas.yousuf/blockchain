const { expect } = require("chai");
const { ethers, upgrades } = require("hardhat");


let Box, box;

describe("text box proxy functionality", () => {

    before(async() => {
        Box = await ethers.getContractFactory("Box");
        box = await upgrades.deployProxy(Box, [10], {initializer: "store"});
    });

    it("retrieve returns a value previously initialized", async() => {
        expect((await box.retrieve())).to.equal(10);
    });

});