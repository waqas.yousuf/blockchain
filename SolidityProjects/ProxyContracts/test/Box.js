const { expect } = require("chai");
const { ethers } = require("hardhat");

let Box, box;

describe("Box contract testing version 1", () => {

  before(async() => {
    Box = await ethers.getContractFactory("Box");
    box = await Box.deploy();
    await box.deployed();
  });

  it("retrieve function returns a value previously stored", async() => {
    const tx = await box.store(10);

    expect(await box.retrieve()).to.equal(10);
  });

});
