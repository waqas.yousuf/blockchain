const { ethers } = require('hardhat');

const main = async() => {
  const Box = await ethers.getContractFactory('Box');
  console.log('Deploying Box contract');
  const box = await upgrades.deployProxy(Box, [10], {initializer: 'store'});
  console.log('Box deployed to: ', box.address);
}



main()
  .then(() => process.exit(0))
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });