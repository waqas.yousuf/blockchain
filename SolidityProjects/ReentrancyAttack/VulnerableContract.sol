// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

contract A is ReentrancyGuard
{
    mapping(address => uint) public balances;

    function deposit() public payable 
    {
        balances[msg.sender] += msg.value;
    }

    function withdraw() public nonReentrant
    {
        uint256 bal = balances[msg.sender];
        // console.log(bal);
        require(bal > 0);

        // point of loop whole
        (bool sent,) = msg.sender.call{value: bal}("");
        require(sent, "Failure");

        // balance is updating in the last
        balances[msg.sender] = 0;
    }

    function getBalance() public view returns(uint)
    {
        return address(this).balance;
    }
}

// step1 deploy Vulnerable  smart contract
// step2 copy address of deployed SC
// step3 deploy B smart contract using address of Vulnerable SC
// step4 deposit ethers into Vulnerable contract
// step5 attack by depositing 1 ether
